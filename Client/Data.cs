﻿using Newtonsoft.Json;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Timers;
using System.Text;
using Microsoft.AspNetCore.SignalR.Client;
using Threading = System.Threading.Tasks;

namespace Client
{
    static class Data
    {
        private static string serverAppPath = "http://localhost:5000/api";

        static HubConnection connection;
        static HttpClient client = new HttpClient();
        public async static void SetConnection()
        {
            connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/project")
                .Build();

            await connection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine($"There was an error opening the connection:{task.Exception.GetBaseException()}");
                }
                else
                {
                    Console.WriteLine("Connected");
                }
            });

            connection.Closed += async (error) =>
            { 
                await Threading.Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            connection.On<string>("GetNotification", message =>
            {
                Console.WriteLine(message);
            });
        }


        public async static Threading.Task<Dictionary<string, int>> GetNumberTasks(int userId)
        {
            return await connection.InvokeAsync<Dictionary<string, int>>("GetNumberTasksInProject", userId);
        }

        public async static Threading.Task<List<Task>> GetListTasks(int userId)
        {
            return await connection.InvokeAsync<List<Task>>("GetListTasks", userId);
        }

        public async static Threading.Task<ICollection<Task>> GetListTasksFinished2019(int userId)
        {
            return await connection.InvokeAsync<ICollection<Task>>("GetListTasksFinished2019", userId);
        }

        public async static Threading.Task<IEnumerable<dynamic>> GetOldUsers()
        {
            return await connection.InvokeAsync<IEnumerable<object>>("GetOldUsers");
        }

        public async static Threading.Task<IEnumerable<Task>> GetSortedUsersList()
        {
            return await connection.InvokeAsync<IEnumerable<Task>>("GetSortedUsersList");
        }
        
        public async static Threading.Task<IEnumerable<dynamic>> GetInfoAboutTasks(int userId)
        {
            return await connection.InvokeAsync<IEnumerable<object>>("GetInfoAboutLastProject", userId);
        }

        public async static Threading.Task<IEnumerable<dynamic>> GetInfoAboutProject(int projectId)
        {
            return await connection.InvokeAsync<dynamic>("GetInfoAboutProject", projectId);
        }

        public async static Threading.Task<IEnumerable<Team>> GetAllTeams()
        {
            return await connection.InvokeAsync<ICollection<Team>>("Get");
        }

        public async static Threading.Task<IEnumerable<User>> GetAllUsers()
        {
            return await connection.InvokeAsync<ICollection<User>>("GetUsers");
        }

        public async static Threading.Task<IEnumerable<Project>> GetAllProjects()
        {
            return await connection.InvokeAsync<ICollection<Project>>("GetProjects");
        }

        public async static Threading.Task<IEnumerable<Task>> GetAllTasks()
        {
            return await connection.InvokeAsync<ICollection<Task>>("GetTasks");
        }

        public async static Threading.Task<Team> GetTeam(int id)
        {
            return await connection.InvokeAsync<Team>("GetById", id);
        }

        public async static Threading.Task<User> GetUser(int id)
        {
            return await connection.InvokeAsync<User>("GetUserById", id);
        }

        public async static Threading.Task<Project> GetProject(int id)
        {
            return await connection.InvokeAsync<Project>("GetProjectById", id);
        }

        public async static Threading.Task<Task> GetTask(int id)
        {
            return await connection.InvokeAsync<Task>("GetTaskById", id);
        }

        public async static Threading.Task Post(string method, object obj)
        {
            await connection.InvokeAsync(method, obj);
        }

        public async static Threading.Task<HttpResponseMessage> Put(string modelName, int id, object obj)
        {
            string jsonObject = JsonConvert.SerializeObject(obj);

            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            return await client.PutAsync($"{serverAppPath}/{modelName}/{id}", content);
        }

        public async static Threading.Task Delete(string method, int id)
        {
            await connection.InvokeAsync(method, id);
        }

        public async static Threading.Task GetLogs()
        {
            await connection.InvokeAsync("GetLogs");
        }

        public async static Threading.Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new Threading.TaskCompletionSource<int>();

            Timer timer = new Timer(delay);
            timer.AutoReset = false;
            timer.Elapsed += async (sender, e) =>
            {
                try
                {
                    tcs.SetResult(await MarkRandomTask());
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };

            timer.Start();

            return await tcs.Task;
        }

        public async static Threading.Task<int> MarkRandomTask()
        {
            var tasks = await GetAllTasks();

            Random random = new Random();
            int idTask = random.Next(1, tasks.Count());
            var randomTask = tasks.ElementAt(idTask - 1);

            randomTask.State = new TaskStateModel { Id = 2, Value = "Finished" };
            if ((await Put("Tasks", idTask, randomTask)).IsSuccessStatusCode)
                    return idTask;
            else throw new Exception();
        }
    }
}
