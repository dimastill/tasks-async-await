﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Data.SetConnection();
            do
            {
                Menu.Display();
                int menuItem = Menu.InputMenuItem();
                Console.Clear();

                switch (menuItem)
                {
                    case 1:
                        Menu.DisplayNumberTasksByUserId();
                        break;
                    case 2:
                        Menu.DisplayListTasksByUserId();
                        break;
                    case 3:
                        Menu.DisplayTasksFinishedIn2019();
                        break;
                    case 4:
                        Menu.DisplayUsersOlderThan12YearsOld();
                        break;
                    case 5:
                        Menu.DisplaySortedUsersList();
                        break;
                    case 6:
                        Menu.DisplayInfoAboutUser();
                        break;
                    case 7:
                        Menu.DisplayInfoAboutProject();
                        break;
                    case 8:
                        Menu.DisplayLogs();
                        break;
                    case 9:
                        Menu.DisplayMarkRandomTask();
                        break;
                }

                Menu.Wait();
            } while (true);
        }
    }
}
