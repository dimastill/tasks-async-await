﻿using ProjectStructureTask.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Command
{
    public class UpdateTeamCommand : ICommand<bool>
    {
        public int UpdateId { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
