﻿using ProjectStructureTask.Services.Abstractions;
using System;
using DataAccess.Models;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Command
{
    public class UpdateTaskCommand : ICommand<bool>
    {
        public int UpdateId { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        public Project Project { get; set; }
        public User Perfomer { get; set; }
    }
}
