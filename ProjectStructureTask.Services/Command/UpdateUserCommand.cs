﻿using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Command
{
    public class UpdateUserCommand : ICommand<bool>
    {
        public int UpdateId { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }
    }
}
