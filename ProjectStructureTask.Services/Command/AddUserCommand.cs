﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;

namespace ProjectStructureTask.Services.Command
{
    public class AddUserCommand : ICommand<bool>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }
    }
}
