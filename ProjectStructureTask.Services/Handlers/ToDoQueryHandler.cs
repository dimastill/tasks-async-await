﻿using AutoMapper;
using DataAccess;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoQueryHandler : IToDoQueryHandler
    {
        private readonly ProjectDbContext dataContext;
        private readonly IMapper mapper;
        private readonly IQueueService queueService;

        public ToDoQueryHandler(ProjectDbContext dataContext, IMapper mapper,
            IQueueService queueService)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
            this.queueService = queueService;
        }


        public Dictionary<Type, Func<object, Threading.Task<object>>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, Threading.Task<object>>>
            {
                { typeof(GetAllProjectsQuery), async t => await HandleAsync(t as GetAllProjectsQuery) },
                { typeof(GetAllTasksQuery), async t => await HandleAsync(t as GetAllTasksQuery) },
                { typeof(GetAllUsersQuery), async t => await HandleAsync(t as GetAllUsersQuery) },
                { typeof(GetAllTeamsQuery), async t => await HandleAsync(t as GetAllTeamsQuery) },
                { typeof(GetProjectQuery), async t => await HandleAsync(t as GetProjectQuery) },
                { typeof(GetTeamQuery), async t => await HandleAsync(t as GetTeamQuery) },
                { typeof(GetUserQuery), async t => await HandleAsync(t as GetUserQuery) },
                { typeof(GetTaskQuery), async t => await HandleAsync(t as GetTaskQuery) },
                { typeof(GetInfoAboutProjectQuery), async t => await HandleAsync(t as GetInfoAboutProjectQuery) },
                { typeof(GetInfoAboutLastProjectQuery), async t => await HandleAsync(t as GetInfoAboutLastProjectQuery) },
                { typeof(GetListTasksFinished2019Query), async t => await HandleAsync(t as GetListTasksFinished2019Query) },
                { typeof(GetListTasksQuery), async t => await HandleAsync(t as GetListTasksQuery) },
                { typeof(GetNumberTasksInProjectQuery), async t => await HandleAsync(t as GetNumberTasksInProjectQuery) },
                { typeof(GetOldUsersQuery), async t => await HandleAsync(t as GetOldUsersQuery) },
                { typeof(GetSortedUsersListQuery), async t => await HandleAsync(t as GetSortedUsersListQuery) },
                
            };
        }

        private async Threading.Task<List<Project>> HandleAsync(GetAllProjectsQuery query)
        {
            await queueService.PostValue("Get all projects was triggered");

            return await dataContext.Projects.ToListAsync();
        }

        private async Threading.Task<List<Task>> HandleAsync(GetAllTasksQuery query)
        {
            await queueService.PostValue("Get all tasks was triggered");
            
            return await dataContext.Tasks.ToListAsync();
        }

        private async Threading.Task<List<User>> HandleAsync(GetAllUsersQuery query)
        {
            await queueService.PostValue("Get all users was triggered");

            return await dataContext.Users.ToListAsync();
        }

        private async Threading.Task<List<Team>> HandleAsync(GetAllTeamsQuery query)
        {
            await queueService.PostValue("Get all teams was triggered");

            return await dataContext.Teams.ToListAsync();
        }

        private async Threading.Task<Project> HandleAsync(GetProjectQuery query)
        {
            await queueService.PostValue($"Get project (id:{query.ProjectId}) was triggered");

            return await Threading.Task.Run(() => 
                dataContext.Projects.Single(findProject => findProject.ProjectId == query.ProjectId));
        }

        private async Threading.Task<Team> HandleAsync(GetTeamQuery query)
        {
            await queueService.PostValue("Get team by id was triggered");

            return await Threading.Task.Run(() => 
                dataContext.Teams.Single(findTeam => findTeam.TeamId == query.Id));
        }

        private async Threading.Task<User> HandleAsync(GetUserQuery query)
        {
            await queueService.PostValue($"Get user (id:{query.Id}) was triggered");

            return await Threading.Task.Run(() => 
                dataContext.Users.Single(findUser => findUser.UserId == query.Id));
        }

        private async Threading.Task<Task> HandleAsync(GetTaskQuery query)
        {
            await queueService.PostValue($"Get task (id:{query.Id}) was triggered");

            return await Threading.Task.Run(() => 
                dataContext.Tasks.Single(findTask => findTask.TaskId == query.Id));
        }

        private async Threading.Task<Dictionary<string, int>> HandleAsync(GetNumberTasksInProjectQuery query)
        {
            await queueService.PostValue("Get task in project was triggered");

            var resultQuery = await Threading.Task.Run(() => dataContext.Projects.GroupJoin(
                dataContext.Tasks.Where(t => t.TaskId == query.UserId),
                p => p, t => t.Project,
                (project, task) => new
                {
                    project,
                    numberTasks = task.Count(t => t.Project.ProjectId == project.ProjectId)
                }));

            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (var item in resultQuery)
            {
                result.Add(item.project.ProjectName, item.numberTasks);
            }

            return result;
        }

        private async Threading.Task<List<Task>> HandleAsync(GetListTasksQuery query)
        {
            await queueService.PostValue($"Get list tasks for user (user id:{query.UserId}) was triggered");
            return await dataContext.Tasks.Where(task => 
                task.Perfomer.UserId == query.UserId && task.TaskName.Length < 45).ToListAsync();
        }

        private async Threading.Task<ICollection<Task>> HandleAsync(GetListTasksFinished2019Query query)
        {
            await queueService.PostValue($"Get task finished in 2019 for user (user id: {query.UserId}) was triggered");

            return await Threading.Task.Run(() => dataContext.Tasks.Where(task => task.Perfomer.UserId == query.UserId && 
                                                                                  task.State.Value == "Finished" &&
                                                                                  task.FinishedAt.Year == 2019)
                                                                   .ToListAsync());
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetOldUsersQuery query)
        {
            await queueService.PostValue("Get old users was triggered");


            var test1 = await Threading.Task.Run(() => dataContext.Users.Where(user =>
                user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null));

            var test2 = await Threading.Task.Run(() => dataContext.Users.Where(user =>
                user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null).OrderBy(user => user.RegisteredAt));

            var oldUsers = await Threading.Task.Run(() => dataContext.Teams.GroupJoin(
                dataContext.Users.Where(user => user.Birthday <= DateTime.Now.AddYears(-12) && user.Team != null)
                                 .OrderBy(user => user.RegisteredAt),
                t => t, u => u.Team,
                (team, user) => new
                {
                    team.TeamId,
                    team.TeamName,
                    Users = user.Select(u => u)
                }));

            return oldUsers;
        }

        private async Threading.Task<IEnumerable<Task>> HandleAsync(GetSortedUsersListQuery query)
        {
            await queueService.PostValue("Get sorted users was triggered");

            return await Threading.Task.Run(() => 
                dataContext.Tasks.OrderBy(task => task.TaskName.Length)
                                 .ThenByDescending(task => task.Perfomer.FirstName));
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetInfoAboutLastProjectQuery query)
        {
            await queueService.PostValue("Get last project was triggered");

            var test1 = dataContext.Projects.Where(p =>
                    p.Author.UserId == query.UserId && p.CreatedAt == dataContext.Projects.Max(x => x.CreatedAt));

            var test2 = test1.Single();

            var test3 = test2.ProjectId;

            var test4 = dataContext.Tasks.Where(t => t.Project.ProjectId == test3);

            var test5 = test4.Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                (y.FinishedAt - y.CreateAt) ? x : y);

            var test = from user in dataContext.Users
                       where user.UserId == query.UserId
                       join project in dataContext.Projects on user.UserId equals project.Author.UserId
                       where project.CreatedAt == dataContext.Projects.Where(p => p.Author.UserId == user.UserId)
                                                          .Max(x => x.CreatedAt)
                       join task in dataContext.Tasks on project.ProjectId equals task.Project.ProjectId

                       let lastProject = project
                       let longTask = dataContext.Tasks.Where(t => t.Project.ProjectId == lastProject.ProjectId)
                                           .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                (y.FinishedAt - y.CreateAt) ? x : y)
                       select longTask;

var result = await Threading.Task.Run(() => 
                         from user in dataContext.Users
                         where user.UserId == query.UserId
                         join project in dataContext.Projects on user.UserId equals project.Author.UserId
                         where project.CreatedAt == dataContext.Projects.Where(p => p.Author.UserId == user.UserId)
                                                            .Max(x => x.CreatedAt)
                         let lastProject = project
                         join task in dataContext.Tasks on project.ProjectId equals task.Project.ProjectId
                         let numberTasksInLastProject = dataContext.Tasks.Count(t => t.Project.ProjectId == lastProject.ProjectId)
                         let numberUnfinishedOrCanceledTasks = dataContext.Tasks.Count(t => 
                            t.Project.ProjectId == lastProject.ProjectId && task.State.Value != "Finished")
                         let longTask = dataContext.Tasks.Where(t => t.Project.ProjectId == lastProject.ProjectId)
                                             .Aggregate((x, y) => (x.FinishedAt - x.CreateAt) >
                                                                  (y.FinishedAt - y.CreateAt) ? x : y)
                         select new
                         {
                             user,
                             lastProject,
                             numberTasksInLastProject,
                             numberUnfinishedOrCanceledTasks,
                             longTask
                         });

            return result;
        }

        private async Threading.Task<IEnumerable<object>> HandleAsync(GetInfoAboutProjectQuery query)
        {
            await queueService.PostValue($"Get info about project (id:{query.ProjectId} was triggered");

            var result = await Threading.Task.Run(() => 
                         from project in dataContext.Projects
                         where project.ProjectId == query.ProjectId
                         join task in dataContext.Tasks on project.ProjectId equals task.Project.ProjectId
                         let longDescriptionProject = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                                                           .Aggregate((x, y) => x.Description.Length >
                                                                                y.Description.Length ? x : y)
                         let shortNameTask = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                                                           .Aggregate((x, y) => x.TaskName.Length <
                                                                                y.TaskName.Length ? x : y)
                         let tasksInProject = dataContext.Tasks.Where(t => t.Project.ProjectId == project.ProjectId)
                         let numberUsersInProject = tasksInProject.Where(t => t.Project.Description.Length > 25 ||
                                                                              tasksInProject.Count() < 3)
                                                                  .Select(t => t.Perfomer).GroupBy(t => t.UserId).Count()
                         select new
                         {
                             project,
                             longDescriptionProject,
                             shortNameTask,
                             numberUsersInProject
                         });

            return result;
        }
    }
}
