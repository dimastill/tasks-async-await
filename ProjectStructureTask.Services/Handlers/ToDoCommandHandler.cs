﻿using AutoMapper;
using DataAccess;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Services.Handlers
{
    public class ToDoCommandHandler : IToDoCommandHandler
    {
        private readonly ProjectDbContext dataContext;
        private readonly IMapper mapper;
        private readonly IQueueService queueService;

        public ToDoCommandHandler(ProjectDbContext dataContext, IMapper mapper,
            IQueueService queueService)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;
            this.queueService = queueService;

            Threading.Task.WaitAll(this.dataContext.LoadData());
        }
        public Dictionary<Type, Func<object, Threading.Task<object>>> GetHandlers()
        {
            return new Dictionary<Type, Func<object, Threading.Task<object>>>
            {
                { typeof(AddProjectCommand), async t => await HandleAsync(t as AddProjectCommand) },
                { typeof(AddTeamCommand), async t => await HandleAsync(t as AddTeamCommand) },
                { typeof(AddUserCommand), async t => await HandleAsync(t as AddUserCommand) },
                { typeof(AddTaskCommand), async t => await HandleAsync(t as AddTaskCommand) },
                { typeof(UpdateProjectCommand), async t => await HandleAsync(t as UpdateProjectCommand) },
                { typeof(UpdateTeamCommand), async t => await HandleAsync(t as UpdateTeamCommand) },
                { typeof(UpdateUserCommand), async t => await HandleAsync(t as UpdateUserCommand) },
                { typeof(UpdateTaskCommand), async t => await HandleAsync(t as UpdateTaskCommand) },
                { typeof(DeleteProjectCommand), async t => await HandleAsync(t as DeleteProjectCommand) },
                { typeof(DeleteUserCommand), async t => await HandleAsync(t as DeleteUserCommand) },
                { typeof(DeleteTeamCommand), async t => await HandleAsync(t as DeleteTeamCommand) },
                { typeof(DeleteTaskCommand), async t => await HandleAsync(t as DeleteTaskCommand) },
            };
        }

        private async Threading.Task<bool> HandleAsync(AddProjectCommand command)
        {
            await queueService.PostValue("Post project was triggered");
            if (await Threading.Task.Run(() => 
                dataContext.Projects.Single(p => p.ProjectId == command.Id)) != null)
                return false;

            var project = mapper.Map<AddProjectCommand, Project>(command);
            await dataContext.Projects.AddAsync(project);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddTeamCommand command)
        {
            await queueService.PostValue("Post team was triggered");

            if (await Threading.Task.Run(() => 
                dataContext.Teams.Where(t => t.TeamId == command.Id) != null))
                return false;

            var team = mapper.Map<AddTeamCommand, Team>(command);
            await dataContext.Teams.AddAsync(team);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddUserCommand command)
        {
            await queueService.PostValue("Post user was triggered");

            if (await Threading.Task.Run(() => 
                dataContext.Users.Where(u => u.UserId == command.Id) != null))
                return false;

            var user = mapper.Map<AddUserCommand, User>(command);

            await dataContext.Users.AddAsync(user);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(AddTaskCommand command)
        {
            await queueService.PostValue("Post task was triggered");

            if (await Threading.Task.Run(() => 
                dataContext.Tasks.Where(t => t.TaskId == command.Id) != null))
                return false;

            var task = mapper.Map<AddTaskCommand, Task>(command);
            await dataContext.Tasks.AddAsync(task);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateProjectCommand command)
        {
            await queueService.PostValue("Put project was triggered");

            var project = await Threading.Task.Run(() => 
                dataContext.Projects.Single(updateProject => updateProject.ProjectId == command.UpdateId));

            if (project == null)
                return false;

            mapper.Map(command, project);
            dataContext.Projects.Update(project);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateTeamCommand command)
        {
            await queueService.PostValue($"Post team (id: {command.TeamId} was triggered");

            var team = await Threading.Task.Run(() => 
                dataContext.Teams.Single(updateTeam => updateTeam.TeamId == command.UpdateId));

            if (team == null)
                return false;

            mapper.Map(command, team);
            dataContext.Teams.Update(team);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateUserCommand command)
        {
            await queueService.PostValue("Put user was triggered");

            var user = await Threading.Task.Run(() => 
                dataContext.Users.Single(updateUser => updateUser.UserId == command.UpdateId));

            if (user == null)
                return false;

            mapper.Map(command, user);
            dataContext.Users.Update(user);
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(UpdateTaskCommand command)
        {
            await queueService.PostValue("Put task was triggered");

            var task = await dataContext.Tasks.SingleAsync(updateTask => 
                updateTask.TaskId == command.UpdateId);

            if (task == null)
                return false;

            task.TaskName = command.TaskName;
            task.Description = command.Description;
            task.CreateAt = command.CreateAt;
            task.FinishedAt = command.FinishedAt;
            task.Perfomer = await dataContext.Users.SingleAsync(u => u.UserId == command.Perfomer.UserId);
            task.Project = await dataContext.Projects.SingleAsync(p => p.ProjectId == command.Project.ProjectId);
            task.State = await dataContext.TaskStateModel.SingleAsync(tsm => tsm.Value == "Finished");

            dataContext.Tasks.Update(task);
            await dataContext.SaveChangesAsync();
            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteProjectCommand command)
        {
            await queueService.PostValue($"Delete project (id:{command.ProjectId}) was triggered");

            var project = await Threading.Task.Run(() => 
                dataContext.Projects.Single(deleteProject => deleteProject.ProjectId == command.ProjectId));

            if (project == null)
                return false;

            await Threading.Task.Run(() => dataContext.Projects.Remove(project));
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteTeamCommand command)
        {
            await queueService.PostValue($"Delete team (id:{command.Id} was triggered");

            var team = await Threading.Task.Run(() => 
                dataContext.Teams.Single(deleteTeam => deleteTeam.TeamId == command.Id));

            if (team == null)
                return false;

            await Threading.Task.Run(() => dataContext.Teams.Remove(team));
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteUserCommand command)
        {
            await queueService.PostValue($"Delete user (id:{command.Id}) was triggered");

            var user = await Threading.Task.Run(() => 
                dataContext.Users.Single(deleteUser => deleteUser.UserId == command.Id));

            if (user == null)
                return false;

            await Threading.Task.Run(() => dataContext.Users.Remove(user));
            dataContext.SaveChanges();

            return true;
        }

        private async Threading.Task<bool> HandleAsync(DeleteTaskCommand command)
        {
            await queueService.PostValue("Delete task was triggered");

            var task = await Threading.Task.Run(() => 
                dataContext.Tasks.Single(deleteTask => deleteTask.TaskId == command.Id));

            if (task == null)
                return false;

            await Threading.Task.Run(() => dataContext.Tasks.Remove(task));
            dataContext.SaveChanges();

            return true;
        }
    }
}
