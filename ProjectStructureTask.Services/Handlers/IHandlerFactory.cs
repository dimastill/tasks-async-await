﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Handlers
{
    public interface IHandlerFactory
    {
        Dictionary<Type, Func<object, Task<object>>> GetHandlers();
    }
}
