﻿using DataAccess.Models;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using Threading = System.Threading.Tasks;

namespace ProjectStructureTask.Services.Hubs
{
    public class ProjectHub : Hub
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        private readonly IQueueService queueService;

        public ProjectHub(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor,
            IQueueService queueService)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
            this.queueService = queueService;
        }

        public async Threading.Task<IEnumerable<Team>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllTeamsQuery());
        }

        public async Threading.Task<IEnumerable<object>> GetOldUsers()
        {
            return await queryProcessor.ProcessAsync(new GetOldUsersQuery());
        }


        public async Threading.Task<Team> GetById(int id)
        {
            return await queryProcessor.ProcessAsync(new GetTeamQuery { Id = id });
        }

        public async Threading.Task<bool> Post(AddTeamCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> PutTeam(int id, UpdateTeamCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteTeamCommand { Id = id });
        }

        public async Threading.Task<Dictionary<string, int>> GetNumberTasksInProject(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetNumberTasksInProjectQuery { UserId = userId });
        }
        public async Threading.Task<List<Task>> GetListTasks(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetListTasksQuery { UserId = userId });
        }

        public async Threading.Task<object> GetListTasksFinished2019(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetListTasksFinished2019Query { UserId = userId });
        }

        public async Threading.Task<IEnumerable<Task>> GetSortedUsersList()
        {
            return await queryProcessor.ProcessAsync(new GetSortedUsersListQuery());
        }

        public async Threading.Task<IEnumerable<object>> GetInfoAboutLastProject(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetInfoAboutLastProjectQuery() { UserId = userId });
        }
        public async Threading.Task<object> GetInfoAboutProject(int projectId)
        {
            return await queryProcessor.ProcessAsync(new GetInfoAboutProjectQuery { ProjectId = projectId });
        }

        public async Threading.Task<IEnumerable<User>> GetUsers()
        {
            return await queryProcessor.ProcessAsync(new GetAllUsersQuery());
        }
        
        public async Threading.Task<User> GetUserById(int id)
        {
            return await queryProcessor.ProcessAsync(new GetUserQuery { Id = id });
        }

        public async Threading.Task<bool> PostUser(AddUserCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> PutUser(int id, UpdateUserCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> DeleteUser(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteUserCommand { Id = id });
        }

        public async Threading.Task<IEnumerable<Project>> GetProjects()
        {
            return await queryProcessor.ProcessAsync(new GetAllProjectsQuery());
        }

        public async Threading.Task<Project> GetProjectById(int id)
        {
            return await queryProcessor.ProcessAsync(new GetProjectQuery { ProjectId = id });
        }

        public async Threading.Task<bool> PostProject(AddProjectCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> PutProject(int id, UpdateProjectCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> DeleteProject(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteProjectCommand { ProjectId = id });
        }

        public async Threading.Task<IEnumerable<Task>> GetTasks()
        {
            return await queryProcessor.ProcessAsync(new GetAllTasksQuery());
        }
        public async Threading.Task<Task> GetTaskById(int id)
        {
             return await queryProcessor.ProcessAsync(new GetTaskQuery { Id = id });
        }
        
        public async Threading.Task<bool> PostTask(AddTaskCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> PutTask(int id, UpdateTaskCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        public async Threading.Task<bool> DeleteTask(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteTaskCommand { Id = id });
        }

        public async Threading.Task<bool> GetLogs()
        {
            return await queueService.PostValue("Get logs");
        }
    }
}
