﻿using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Abstractions
{
    public interface ICommandProcessor
    {
        Task<TResult> ProcessAsync<TResult>(ICommand<TResult> command);
    }
}