﻿using Worker.Interfaces;
using ProjectStructureTask.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureTask.Services.Hubs;
using Worker.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.IO;
using System.Threading.Tasks;

namespace ProjectStructureTask.Services.Implementation
{
    public class QueueService : IQueueService
    {
        private readonly IMessageProducerScope messageProducerScope;
        private readonly IMessageConsumerScope messageConsumerScope;

        private readonly IHubContext<ProjectHub> projectHub;

        public QueueService(
            IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IHubContext<ProjectHub> hubContext)
        {
            projectHub = hubContext;

            messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });

            messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public async Task<bool> PostValue(string value)
        {
            try
            {
                await messageProducerScope.MessageProducer.SendAsync(value);  
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async void GetValue(object sender, BasicDeliverEventArgs args)
        {
            var value = Encoding.UTF8.GetString(args.Body);
            
            Console.WriteLine($"\n\n{value}\n\n");

            await projectHub.Clients.All.SendAsync("GetNotification", value);

            await messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);
        }
    }
}
