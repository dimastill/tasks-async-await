﻿using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;

namespace ProjectStructureTask.Services.Queries
{
    public class GetTaskQuery : IQuery<Task>
    {
        public int Id { get; set; }
    }
}
