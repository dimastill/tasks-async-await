﻿using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;

namespace ProjectStructureTask.Services.Queries
{
    public class GetTeamQuery : IQuery<Team>
    {
        public int Id { get; set; }
    }
}
