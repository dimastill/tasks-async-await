﻿using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetNumberTasksInProjectQuery : IQuery<Dictionary<string, int>>
    {
        public int UserId { get; set; }
    }
}
