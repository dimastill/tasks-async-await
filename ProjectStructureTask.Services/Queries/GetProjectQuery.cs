﻿using ProjectStructureTask.Services.Abstractions;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetProjectQuery : IQuery<Project>
    {
        public int ProjectId { get; set; }
    }
}
