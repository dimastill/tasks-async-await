﻿using DataAccess.Models;
using ProjectStructureTask.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureTask.Services.Queries
{
    public class GetListTasksFinished2019Query : IQuery<ICollection<Task>>
    {
        public int UserId { get; set; }
    }
}
