﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;

        public UsersController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllUsersQuery());
        }

        [Route("sorted")]
        [HttpGet]
        public async Task<IEnumerable<DataAccess.Models.Task>> GetSortedUsersList()
        {
            return await queryProcessor.ProcessAsync(new GetSortedUsersListQuery());
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<User> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetUserQuery { Id = id });
        }

        [Route("TasksInProjectForUser/{userId}")]
        [HttpGet("{userId}", Name = "GetTaskInProjectForUser")]
        public async Task<Dictionary<string, int>> GetNumberTasksInProject(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetNumberTasksInProjectQuery { UserId = userId });
        }

        // POST: api/Users
        [HttpPost]
        public async Task<bool> Post([FromBody] AddUserCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<bool> Put(int id, [FromBody] UpdateUserCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteUserCommand { Id = id });
        }
    }
}
