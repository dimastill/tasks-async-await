﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;

        public TasksController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<IEnumerable<DataAccess.Models.Task>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllTasksQuery());
        }

        // GET: api/Tasks/5
        [HttpGet("{id}", Name = "GetTask")]
        public async Task<DataAccess.Models.Task> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetTaskQuery { Id = id });
        }

        [Route("ForUser/{userId}")]
        [HttpGet("{userId}", Name = "GetListTasksForUser")]
        public async Task<List<DataAccess.Models.Task>> GetListTasks(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetListTasksQuery { UserId = userId });
        }

        [Route("FinishedIn2019/{userId}")]
        [HttpGet("{userId}", Name = "GetFinishedTask")]
        public async Task<object> GetListTasksFinished2019(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetListTasksFinished2019Query { UserId = userId });
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task<bool> Post([FromBody] AddTaskCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public async Task<bool> Put(int id, [FromBody] UpdateTaskCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteTaskCommand { Id = id });
        }
    }
}
