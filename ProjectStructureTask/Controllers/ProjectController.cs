﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Queries;

namespace ProjectStructureTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ICommandProcessor commandProcessor;
        private readonly IQueryProcessor queryProcessor;
        public ProjectController(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor)
        {
            this.commandProcessor = commandProcessor;
            this.queryProcessor = queryProcessor;
        }
        // GET: api/Project
        [HttpGet]
        public async Task<IEnumerable<Project>> Get()
        {
            return await queryProcessor.ProcessAsync(new GetAllProjectsQuery());
        }

        // GET: api/Project/5
        [HttpGet("{id}", Name = "GetProject")]
        public async Task<Project> Get(int id)
        {
            return await queryProcessor.ProcessAsync(new GetProjectQuery { ProjectId = id });
        }

        [Route("GetInfoAboutProject/{projectId}")]
        [HttpGet("{projectId}", Name = "GetInfoAboutProject")]
        public async Task<object> GetInfoAboutProject(int projectId)
        {
            return await queryProcessor.ProcessAsync(new GetInfoAboutProjectQuery { ProjectId = projectId });
        }

        [Route("LastProject/{userId}")]
        [HttpGet("{userId}", Name = "GetInfoAboutLastProject")]
        public async Task<IEnumerable<object>> GetInfoAboutLastProject(int userId)
        {
            return await queryProcessor.ProcessAsync(new GetInfoAboutLastProjectQuery() { UserId = userId });
        }

        // POST: api/Project
        [HttpPost]
        public async Task<bool> Post([FromBody] AddProjectCommand command)
        {
            return await commandProcessor.ProcessAsync(command);
        }

        // PUT: api/Project/5
        [HttpPut("{id}")]
        public async Task<bool> Put(int id, [FromBody] UpdateProjectCommand command)
        {
            command.UpdateId = id;
            return await commandProcessor.ProcessAsync(command);
        }

        // DELETE: api/Project/0
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await commandProcessor.ProcessAsync(new DeleteProjectCommand { ProjectId = id });
        }
    }
}
