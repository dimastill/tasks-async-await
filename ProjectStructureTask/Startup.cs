﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructureTask.Services.Abstractions;
using ProjectStructureTask.Services.Command;
using ProjectStructureTask.Services.Handlers;
using ProjectStructureTask.Services.Hubs;
using ProjectStructureTask.Services.Implementation;
using ProjectStructureTask.Services.Repositories;
using RabbitMQ.Client;
using Worker.QueueServices;
using Worker.Interfaces;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using DataAccess.Models;

namespace ProjectStructureTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);


            services.AddScoped<IQueueService, QueueService>();

            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory, Worker.QueueServices.ConnectionFactory>();

            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>();

            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();

            services.AddSingleton<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IToDoQueryHandler, ToDoQueryHandler>();
            services.AddTransient<IToDoCommandHandler, ToDoCommandHandler>();
            services.AddTransient<ICommandProcessor, ToDoCommandProcessor>();
            services.AddTransient<IQueryProcessor, ToDoQueryProcessor>();

            services.AddDbContext<ProjectDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ProjectDatabase")));

            services.AddCors();
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .WithOrigins("http://localhost:5000"));

            app.UseSignalR(routes =>
            {
                routes.MapHub<ProjectHub>(path: "/project");
            });

            app.UseMvc();
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AddProjectCommand, Project>();
                cfg.CreateMap<AddTeamCommand, Team>();
                cfg.CreateMap<AddUserCommand, User>();
                cfg.CreateMap<AddTaskCommand, DataAccess.Models.Task>();
                cfg.CreateMap<UpdateProjectCommand, Project>();
                cfg.CreateMap<UpdateTeamCommand, Team>();
                cfg.CreateMap<UpdateUserCommand, User>();
                cfg.CreateMap<UpdateTaskCommand, DataAccess.Models.Task>();
            });

            return config;
        }
    }
}
