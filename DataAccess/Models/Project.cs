﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Project
    {
        public int ProjectId { get; set; }
        [Required]
        public string ProjectName { get; set; }
        [MinLength(10)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime Deadline { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
