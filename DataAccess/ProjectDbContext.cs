﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Threading = System.Threading.Tasks;

namespace DataAccess
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base (options)
        {
            Threading.Task.WaitAll(InitializeDb());
        }

        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskStateModel> TaskStateModel { get; set; }

        public async Threading.Task<int> InitializeDb()
        {
            await Threading.Task.Delay(1000);

            if(await Database.EnsureCreatedAsync())
                await Database.MigrateAsync();

            await LoadData();

            var teams = new List<Team> {
                    new Team {CreatedAt = DateTime.Parse("27.07.1968"), TeamName = "TeamNameTest0" },
            };

            var users = new List<User>
            {
                    new User {FirstName = "FirstNameTest0", LastName = "LastNameTest0",
                        Email = "EmailTest0@email.com", Birthday = DateTime.Parse("21.05.1972"), Team = teams[0]
                    },
                    new User {FirstName = "FirstNameTest1", LastName = "LastNameTest1",
                        Email = "EmailTest1@email.com", Birthday = DateTime.Parse("12.01.2005"), Team = teams[0]
                    },
            };

            var projects = new List<Project>
            {
                    new Project {ProjectName = "NameTest0", CreatedAt = DateTime.Parse("03.06.1962"),
                        Deadline = DateTime.Parse("23.09.1970"), Description = "DescriptionTest0",
                        Team = teams[0], Author = users[0]
                    },
                    new Project {ProjectName = "NameTest1", CreatedAt = DateTime.Parse("30.12.1979"),
                        Deadline = DateTime.Parse("22.06.1986"), Description = "DescriptionTest1",
                        Team = teams[0], Author = users[1]
                    },
                    new Project {ProjectName = "NameTest2", CreatedAt = DateTime.Parse("03.07.2001"),
                        Deadline = DateTime.Parse("03.10.1971"), Description = "DescriptionTest2",
                        Team = teams[0], Author = users[0]
                    },
            };

            var tasks = new List<Task>
                {
                    new Task {Description = "DescriptionTest0", TaskName = "NameTest0",
                        CreateAt = DateTime.Parse("25.08.1987"), FinishedAt = DateTime.Parse("05.11.1988"),
                        Perfomer = users[0], Project = projects[0], State = new TaskStateModel { Id = 0, Value = "Created" }
                    },
                    new Task {Description = "DescriptionTest1", TaskName = "NameTest1",
                        CreateAt = DateTime.Parse("10.06.1997"), FinishedAt = DateTime.Parse("23.03.2005"),
                        Perfomer = users[0], Project = projects[1], State = new TaskStateModel { Id = 0, Value = "Started" }
                    },
                    new Task {Description = "DescriptionTest2", TaskName = "NameTest2",
                        CreateAt = DateTime.Parse("17.08.1962"), FinishedAt = DateTime.Parse("12.03.1969"),
                        Perfomer = users[1], Project = projects[2], State = new TaskStateModel { Id = 0, Value = "Finished" }
                    },
                    new Task {Description = "DescriptionTest3", TaskName = "NameTest3",
                        CreateAt = DateTime.Parse("24.09.1972"), FinishedAt = DateTime.Parse("15.06.1990"),
                        Perfomer = users[1], Project = projects[0], State = new TaskStateModel { Id = 0, Value = "Canceled" }
                    }
                };

            if (await Teams.CountAsync() == 0)
            {
                Teams.AddRange(teams);
            }

            if (await Users.CountAsync() == 0)
            {
                Users.AddRange(users);
            }

            if (await Projects.CountAsync() == 0)
            {
                Projects.AddRange(projects);
            }

            if (await Tasks.CountAsync() == 0)
            {
                Tasks.AddRange(tasks);
            }

            return await SaveChangesAsync();
        }

        public async Threading.Task LoadData()
        {
            await Teams.LoadAsync();
            await Users.LoadAsync();
            await Projects.LoadAsync();
            await Tasks.LoadAsync();
            await TaskStateModel.LoadAsync();
        }
    }
}
