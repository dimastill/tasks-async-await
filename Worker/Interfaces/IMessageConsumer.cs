﻿using RabbitMQ.Client.Events;
using System;
using System.Threading.Tasks;

namespace Worker.Interfaces
{
    public interface IMessageConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        Task Connect();
        Task SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
