﻿using System;
using System.Threading.Tasks;

namespace Worker.Interfaces
{
    public interface IMessageProducer
    {
        Task SendAsync(string message, string type = null);
        Task SendTypedAsync(Type type, string message);
    }
}
