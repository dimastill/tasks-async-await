﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Worker.Interfaces;
using Worker.Models;

namespace Worker
{
    class MessageService : IDisposable
    {
        private readonly IMessageConsumerScope messageConsumerScope;
        private readonly IMessageProducerScope messageProducerScope;

        public MessageService(IMessageConsumerScopeFactory messageConsumerScopeFactory,
            IMessageProducerScopeFactory messageProducerScopeFactory)
        {
            messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "*.queue.#"
            });

            messageConsumerScope.MessageConsumer.Received += MessageReceived;

            messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ClientExchnge",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
        }

        public void Run()
        {
            Console.WriteLine("Service run");
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;

            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                var log = new Log
                {
                    Message = value,
                    DateTime = DateTime.Now
                };
                using (StreamWriter streamWriter = new StreamWriter("log.txt", true))
                {
                    streamWriter.WriteLine($"{log.DateTime} {log.Message}");
                }
                Console.WriteLine($"Received: {value}");
                processed = true;

                if (value == "Get logs")
                    value = File.ReadAllText("log.txt");

                SendState(processed, value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                processed = false;
                SendState(processed);
            }
            finally
            {
                messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        private void SendState(bool state, string receiveValue = null)
        {
            if (state)
                messageProducerScope.MessageProducer.SendAsync($"Successfully received: {receiveValue}");
            else
                messageProducerScope.MessageProducer.SendAsync($"Failed during handling a message");
        }

        public void Dispose()
        {
            messageConsumerScope.Dispose();
            messageProducerScope.Dispose();
        }
    }
}
