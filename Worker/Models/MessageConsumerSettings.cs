﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Models
{
    public class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; }
        public bool AutoAcknowledge { get; set; }
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}
