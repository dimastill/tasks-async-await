﻿using System;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;

namespace Worker.QueueServices
{
    public class ConnectionFactory : RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(IConfiguration config)
        {
            Uri = new Uri(config.GetSection(key: "Rabbit").Value);
            RequestedConnectionTimeout = 30000;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(30);
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            RequestedHeartbeat = 60;
        }

        public override IConnection CreateConnection()
        {
            return base.CreateConnection();
        }
    }
}
