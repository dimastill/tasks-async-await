﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Worker.Interfaces;
using Worker.Models;

namespace Worker.QueueServices
{
    public class MessageProducer : IMessageProducer
    {
        private readonly MessageProducerSettings _messageProducerSettings;
        private readonly IBasicProperties _properties;

        public MessageProducer(MessageProducerSettings messageProducerSettings)
        {
            _messageProducerSettings = messageProducerSettings;

            _properties = messageProducerSettings.Channel.CreateBasicProperties();
            _properties.Persistent = true;
        }

        public async Task SendAsync(string message, string type = null)
        {
            if (!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            await Task.Run(() => _messageProducerSettings.Channel.BasicPublish(
                _messageProducerSettings.PublicationAddress, _properties, body));
        }

        public async Task SendTypedAsync(Type type, string message)
        {
            await SendAsync(message, type.AssemblyQualifiedName);
        }
    }
}
